@push('scripts')     
    <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins/select2/select2.min.js") }}'></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#parent').select2({
                placeholder: "Search for a category",
                ajax: {
                  url: '{{ route("admin.user.filter") }}',
                  dataType: 'json',
                  processResults: function (data) {
                    return {
                      results: data
                    };
                  },
                  cache: true          
                }
              });
        });
        
    </script>
@endpush
<div id="editModal" class="modal fade modal-info" role="dialog">
        <div class="modal-dialog modal-lg">
        
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Category</h4>
                </div>
            <div class="modal-body">                    
                <div class="row">
                    <div class="container">
                        <div class="col-md-3">Title:</div>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="title"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="container">
                            <div class="col-md-3">Parent:</div>
                            <div class="col-md-9">                                
                                <select class="form-control select2" id="parent"  name="parent"></select>                                  
                            </div>
                        </div>
                </div>
                <div class="row">
                        <div class="container">
                            <div class="col-md-3">Icon:</div>
                            <div class="col-md-9">
                                <input type="hidden" name="icon"/>
                                <img src="{{ asset('img/no-image.png')}}" id="simpleButton"/>                                
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </div>        
        </div>
</div>

