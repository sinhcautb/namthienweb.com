<!DOCTYPE html>
<html lang="en">
<head>        
    <title>@yield('title')</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="image/ico" href="{{ asset('themes/taurus/favicon.ico') }}"/>
    
    <link href="{{ asset('themes/taurus/css/stylesheets.css') }}" rel="stylesheet" type="text/css" />        
    
    <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins/jquery/jquery.min.js") }}'></script>
    <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins/jquery/jquery-ui.min.js") }}'></script>
    <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins/jquery/jquery-migrate.min.js") }}'></script>
    <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins/jquery/globalize.js") }}'></script>    
    <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins/bootstrap/bootstrap.min.js") }}'></script>  
    <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins.js") }}'></script>    
    <script type='text/javascript' src='{{ asset("themes/taurus/js/actions.js") }}'></script>    
    <script type='text/javascript' src='{{ asset("themes/taurus/js/charts.js") }}'></script>
    <script type='text/javascript' src='{{ asset("themes/taurus/js/settings.js") }}'></script>
    <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins/uniform/jquery.uniform.min.js") }}'></script>
    <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js") }}'></script>    
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        </script>
        
    @stack('scripts')
    
</head>
<body class="bg-img-num1">     
    <div class="container">        
        <div class="row">                   
            <div class="col-md-12">                
                 <nav class="navbar brb" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-reorder"></span>                            
                        </button>                                                
                        <a class="navbar-brand" href="index.html"><img src="{{ asset('themes/taurus/img/logo.png') }}"/></a>                                                                                     
                    </div>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">                                     
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="{{ route('admin.home') }}">
                                    <span class="icon-home"></span> dashboard
                                </a>
                            </li>      
                            <li>
                                <a href="{{ route('admin.category') }}">                                        
                                    <span class="icon-home"></span>categories
                                </a>
                            </li>       
                                                                
                        </ul>                                                                   
                    </div>
                </nav>   
            </div>            
        </div>
        <div class="row">          
            @yield('content')
        </div>
        <div class="row">
            <div class="page-footer">
                <div class="page-footer-wrap">
                    <div class="side pull-left">
                        Copyirght &COPY; YourCompany 2013. All rights reserved.
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>