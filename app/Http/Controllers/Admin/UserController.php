<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    protected $userMd;
    public function __construct()
    {
        $this->userMd =  new User();
    }
    public function filter(Request $request)
    {   
        $users = $this->userMd->filter($request);
        return response($users,200)->header('Content-Type', 'text/plain');
    }
}
