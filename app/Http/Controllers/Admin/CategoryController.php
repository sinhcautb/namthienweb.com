<?php 

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller {
    public function listView(){       
        $categories = Category::paginate(5);               
        return view('admin.category.list',array(
            'categories'=>$categories
        ));
    }    
    
}